#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

readonly pstart="$PWD"
export PATH="$PATH:$pstart/glslang/bin"

# build opengothic
#
pushd "source"
cmake -H. -Bbuild -DBUILD_EXTRAS:BOOL=OFF -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo -DBUILD_SHARED_MOLTEN_TEMPEST:BOOL=ON
cmake --build ./build --target all
popd

mkdir "39510/dist/lib/"
mkdir "39510/dist/bin/"
cp -rfv "source/build/opengothic/Gothic2Notr" "39510/dist/bin/Gothic2Notr"
cp -rfv "source/build/opengothic/Gothic2Notr.sh" "39510/dist/Gothic2Notr.sh"
cp -rfv source/build/opengothic/*.so* "39510/dist/lib/"
cp -rfv run-gothic2.sh "39510/dist/run-gothic2.sh"
